package com.epam.uni.testsAtClass;


import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class Homework extends TestBase {
	//public static void main(String [] args) {
//	private final String Trello_URL = "https://trello.com/login";
	
	public Homework() {
		super();
	}
	
	@Test
    public void loginShouldWorkTest() throws Exception
    {
		//#content > div > div.js-boards-page > div > div > div > div > div.all-boards > div > div > div.boards-page-board-section.mod-no-sidebar > ul > li:nth-child(3) > a
		//driver.get(Trello_URL);
		driver.navigate().to("https://trello.com/login"); 
		//waiting some time
		
        driver.findElement(By.cssSelector("#user")).sendKeys("pazmany.webdriver@gmail.com");
        driver.manage().timeouts().implicitlyWait(4,TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("#password")).sendKeys("WebDriver");
        driver.manage().timeouts().implicitlyWait(4,TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("#login")).click();
        
       
        driver.findElement(By.cssSelector("#content > div > div.js-boards-page > div > div > div > div > div.all-boards > div > div > div.boards-page-board-section.mod-no-sidebar > ul > li:nth-child(1) > a > div")).click();
        
        //click on card
        driver.findElement(By.cssSelector("#board > div:nth-child(1) > div.list.js-list-content > a")).click();

        
        //after opening the card filed write the values
 
        driver.findElement(By.cssSelector("#board > div:nth-child(1) > div.list.js-list-content > div.list-cards.u-fancy-scrollbar.u-clearfix.js-list-cards.js-sortable.ui-sortable > div > div.list-card.js-composer > div > textarea")).sendKeys("MK:2018-11-26 11:42");


       
        //click on the selector card to add the monogram
        driver.findElement(By.cssSelector("#board > div:nth-child(1) > div.list.js-list-content > div.list-cards.u-fancy-scrollbar.u-clearfix.js-list-cards.js-sortable.ui-sortable > div > div.cc-controls.u-clearfix > div:nth-child(1) > input")).click();
        

        
        
        //add a new list named done click add another card
        driver.findElement(By.cssSelector("#board > div.js-add-list.list-wrapper.mod-add.is-idle > form > a > span > span")).click();
        
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        
        //text area for writing
        driver.findElement(By.cssSelector("#board > div.js-add-list.list-wrapper.mod-add > form > input")).sendKeys("Done");
        
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        
         //click on add Done  list
        driver.findElement(By.cssSelector("#board > div.js-add-list.list-wrapper.mod-add > form > div > input")).click();
            
       
       
 //drag and drop
///////////////////
//#board > div:nth-child(1) > div.list.js-list-content > div.list-cards.u-fancy-scrollbar.u-clearfix.js-list-cards.js-sortable.ui-sortable > a:nth-child(1) > div.list-card-details.js-card-details
    String Cards="#board > div:nth-child(1) > div.list.js-list-content > div.list-cards.u-fancy-scrollbar.u-clearfix.js-list-cards.js-sortable.ui-sortable > a:nth-child(1) > div.list-card-details.js-card-details";
    //String Cards="#list-card-members js-list-card-members";
    //div.list-card-details.js-card-details
    //<div class="list-card-details js-card-details"><div class="list-card-labels js-card-labels"></div><span class="list-card-title js-card-name" dir="auto"><span class="card-short-id hide">#84</span>MK:2018-11-26 11:42</span><div class="badges"><span class="js-badges"></span><span class="custom-field-front-badges js-custom-field-badges"><span></span></span><span class="js-plugin-badges"><span></span></span></div><div class="list-card-members js-list-card-members"></div></div>
    //WebElement source = (WebElement) driver.findElements(By.linkText(Cards));
    WebElement source =driver.findElement(By.cssSelector(Cards));
    
    String Cards2="#board > div:nth-child(2) > div > div.list-header.js-list-header.u-clearfix.is-menu-shown > div.list-header-target.js-editing-target";
    //String Cards2="#div.list-header-target.js-editing-target";
	WebElement target = driver.findElement(By.cssSelector(Cards2));
	/////////////////
    Actions builder = new Actions(driver);
    

   
    builder.keyDown(Keys.SHIFT).click(source).keyUp(Keys.SHIFT).release(target).build().perform();

    
    driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
    
   // Timeouts WaitTime = driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
   
    //assert true
    //#board > div:nth-child(1) > div.list.js-list-content > div.list-cards.u-fancy-scrollbar.u-clearfix.js-list-cards.js-sortable.ui-sortable > a:nth-child(12) > div.list-card-details.js-card-details
   // String cardData ="#board > div:nth-child(1) > div.list.js-list-content > div.list-cards.u-fancy-scrollbar.u-clearfix.js-list-cards.js-sortable.ui-sortable > a:nth-child(12) > div.list-card-details.js-card-details";
    //((Object) WaitTime).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cardData)));
	//driver.findElement(By.linkText(cardData)).click();
	//String cardList = driver.findElement(By.cssSelector("#board > div.js-add-list.list-wrapper.mod-add > form > input")).getText();
	//Assert.assertTrue(cardList.equals("Done"));

    
    ///////////////////////
   /* 
    //Locate the draggable element
    WebElement draggable = driver.findElement(By.xpath(xp1));
    //Locate the droppable element
    WebElement droppable = driver.findElement(By.xpath(xp2));
    //Add the drag and drop action here
    new Actions(driver).dragAndDrop(draggable, droppable).build().perform();
    
    Assert.assertTrue(driver.findElement(By.cssSelector("#droppable > p:nth-child(1)")).getText().equals("Dropped!"));
    */
    // driver.close();
     //driver.quit();
	
}
}



